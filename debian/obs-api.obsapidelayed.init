#!/bin/bash
# Copyright (c) 2009 SUSE Linux AG, Nuernberg, Germany.
# All rights reserved.
#
# Author: Adrian Schroeter
# Please send feedback to http://www.suse.de/feedback/
#
# /etc/init.d/obsapidelayed
#
#    This program is free software; you can redistribute it and/or modify 
#    it under the terms of the GNU General Public License as published by 
#    the Free Software Foundation; either version 2 of the License, or 
#    (at your option) any later version. 
# 
#    This program is distributed in the hope that it will be useful, 
#    but WITHOUT ANY WARRANTY; without even the implied warranty of 
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
#    GNU General Public License for more details. 
# 
#    You should have received a copy of the GNU General Public License 
#    along with this program; if not, write to the Free Software 
#    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
#
### BEGIN INIT INFO
# Provides:          obsapidelayed
# Required-Start:    $network $remote_fs $syslog
# Required-Stop:     $remote_fs $syslog
# Should-Start:      obsapisetup
# Should-Stop:       $none
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: OBS api delayed jobs
# Description:       Start the delayed job handler for OBS api
### END INIT INFO


API_ROOT=/usr/share/obs/api
CLOCKWORKD=/usr/bin/clockworkd

. /lib/lsb/init-functions

# If you are using newrelic_rpm you need this
# to fix detection of our delay_job.api daemons
export NEW_RELIC_DISPATCHER=delayed_job

function run_in_api
{
  export RAILS_ENV="production"
  # startproc only works for classical daemons
  chroot --userspec=www-data:www-data / /bin/bash -c "cd $API_ROOT && /usr/bin/bundle exec $*"
}

# number of parallel delayed jobs
NUM=3

case "$1" in
    start)
	echo -n "Starting OBS api delayed job handler "
        run_in_api script/delayed_job.api.rb --queue=quick start -n $NUM
        run_in_api script/delayed_job.api.rb --queue=releasetracking start -i 1000
        run_in_api script/delayed_job.api.rb --queue=issuetracking start -i 1010
        run_in_api script/delayed_job.api.rb --queue=mailers start -i 1020
        # to be removed later. This is just for processing old jobs without default queue
        run_in_api script/delayed_job.api.rb start -i 1030
	echo -n "Starting OBS api clock daemon "
        run_in_api $CLOCKWORKD --log-dir=log -l -c config/clock.rb -d $API_ROOT start
        # searchd got started by clockd
	;;
    stop)
	echo -n "Shutting down OBS api delayed job handler "
        run_in_api script/delayed_job.api.rb --queue=quick stop -n $NUM
        run_in_api script/delayed_job.api.rb --queue=releasetracking stop -i 1000
        run_in_api script/delayed_job.api.rb --queue=issuetracking stop -i 1010
        run_in_api script/delayed_job.api.rb --queue=mailers stop -i 1020
        # to be removed later. This is just for processing old jobs without default queue
        run_in_api script/delayed_job.api.rb stop -i 1030
	echo -n "Shutting down OBS api clock daemon "
        run_in_api $CLOCKWORKD -l -c config/clock.rb stop
	echo -n "Shutting down OBS searchd daemon "
        run_in_api rake ts:stop
	;;
    try-restart|condrestart)
	if test "$1" = "condrestart"; then
		echo "${attn} Use try-restart ${done}(LSB)${attn} rather than condrestart ${warn}(RH)${norm}"
	fi
	$0 status
	if test $? = 0; then
		$0 restart
	fi
	;;
    clean-restart)
	$0 stop
        rm -f $API_ROOT/db/sphinx/production/*
        run_in_api rake ts:index
	$0 start

	;;
    restart)
	$0 stop
	$0 start

	;;
    force-reload)
	echo -n "Reload service OBS api delayed jobs "
	$0 try-restart
	;;
    status)
	echo -n "Checking for service delayed OBS api jobs "
		"W: not implemented"
        [ $? == $NUM ]
	;;
    *)
	echo "Usage: $0 {start|stop|status|try-restart|restart|force-reload}"
	exit 1
	;;
esac
